**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

## Ionic
Ionic is the open-source mobile app development framework that makes it easy to build top quality native and progressive web apps with web technologies.

##Ionic version used is 3


## Project Structure
 Main source code is withing the scr folder
  the pages folder contains a folder called twitter-feed-ionic that has all the logic
  the providers folder contains a folder called -user-content which contains code logic for loading the mock data from .txt files.
  the TwitterFeed\src\pages\twitter-feed-ionic directory contains twitter-feed.spec.ts file which has unit tests for the twitterFeed component
  the TwitterFeed\src\providers\user-content directory contains user-content.spect.ts file which has unit tests for the loading .txt mock data

production bundle is contained within TwitterFeed\www\build
  
 
## runnig the app.
1. Make sure that node.js is installed on your mac/pc
2. Open the app using any web app code editor, e.g VS Code, Atom or Brackets
3. launch your command-line and navigate to the project directory
4. from the command-line execute the 'npm install' command within project directory. This will insatall all dependencies used for the app
5. from the command-limne, execute the command 'ionic serve'. This will launch the app on your browser.
6. from the browser press f12 to view your console.

## building the app for production
1. from the command line within the project directory, execute the command ionic build --prod
    this command will create a fresh build folder under TwitterFeed\www\
    
    
## running tests
1 by executing the tests. From the command line within the project directory,execute the command npm test
