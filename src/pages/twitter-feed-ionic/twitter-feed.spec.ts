import { TwitterFeedPage } from './twitter-feed';
import {} from 'jasmine';
 
describe('load twitter user and their tweets', () => {

    var twitterTests;
    var users:any ;
    var followers: any ;

    beforeEach(() => {
    twitterTests =   TwitterFeedPage;
    users = ['Drenco', 'Pete', 'Newman'];
    followers = ['Gregory,Romans,Newman','Raymond','Sam']
  });
 
    it('should isolate followers user names which are on a single line separated by a comma ', () => {
         
      const result = twitterTests.separateUsers(users,followers,0);
      expect(result).not.toContain(',');
      
    });

    
    it('should removes null, 0, false, undefined and NaN values from an array', () => {
 
      const result = twitterTests.filter_array(users);
      expect(result).not.toContain(',');
      
    });
 
});