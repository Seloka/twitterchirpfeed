import { Component } from '@angular/core';
import {UserContentProvider} from '../../providers/user-content/user-content' 
import 'rxjs/Rx'; 
import * as _ from "lodash";

@Component({
  selector: 'page-hello-ionic',
  templateUrl: 'twitter-feed.html'
})
export class TwitterFeedPage {
  userResults: any =[];
  users: any=[]; //this stores all avaliable users
  follows: any=[];
  tweetResults: any =[];
  tweets: any=[];

  constructor(public userdata: UserContentProvider) {}

  ngOnInit() {
    console.clear()
    /*promise for calling all users service*/ 
    this.userdata.loadUsers().then(data => {  
      this.userResults = data.text().split('\n'); 
      this.userResults = this.filter_array(this.userResults) 
      for (var i = 0; i < this.userResults.length; i++) { 
        this.users.push(this.userResults[i].substring(0, this.userResults[i].indexOf('follows')).trim()); 
        this.follows.push(this.userResults[i].substring(this.userResults[i].indexOf('follows')+"follows".length).trim());  
        if(this.follows[i].includes(',')){  
            this.separateUsers(this.users,this.follows,i)
        }
      }    
     }).catch(err => console.log(err));
 
     /*promise for calling tweets service*/
    this.userdata.loadTweets().then(data => { 
      this.users = this.filter_array(this.users)
      if (this.users.length == 0) {
        console.log("User 's file is empty or corrupt")
      }
      else {
        this.tweetResults = data.text().split('\n');
        this.tweetResults = this.filter_array(this.tweetResults)

        this.users = Array.from(new Set(this.users)); //Removes any possible duplicate users in the array
        this.users.sort() // sort 
        for (var i = 0; i < this.tweetResults.length; i++) {
          this.tweets.push(this.tweetResults[i].substring(this.tweetResults[i].indexOf('>') + 1));
        } 
        this.displayTweetes(this.users,this.tweets,this.tweetResults);
      }
     }).catch(err => console.log(err));;

  }

  public separateUsers(userArr,followersArr,arryIdx){
     /**
     * isolates comma separated values.
     */
    let temp: any = [];
    temp = followersArr[arryIdx].split(',');
    for (var idx = 0; idx < temp.length; idx++) {
      userArr.push(temp[idx].trim());
    } 
  }

  private filter_array(test_array) {
    /**
     * removes 'null', '0', '""', 'false', 'undefined' and 'NaN' values from an array.
     */
    var index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        var value = test_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}

  private displayTweetes(users,allTweets,myFollowerTweets){ 
     /**
     * display users, their tweets and their followers 's tweets .
     */
    for (var i = 0; i < this.users.length; i++) {
      console.log(this.users[i]);
      for (var j = 0; j < allTweets.length; j++) {
        if (this.users[i].trim() === myFollowerTweets[j].substring(0, myFollowerTweets[j].indexOf('>'))) {
          let myFollowers: any = [];
          console.log("@" + this.users[i].trim() + ":" + myFollowerTweets[j].substring(myFollowerTweets[j].indexOf('>') + 1));
          for (var p = 0; p < this.follows.length; p++) {
            if (this.follows[p].includes(',')) {
              let temp: any = [];
              temp = this.follows[p].split(',');
              myFollowers = []
              for (var idx = 0; idx < temp.length; idx++) {
                myFollowers.push(temp[idx].trim());
              }
            }
            else {
              myFollowers.push(this.follows[p]);
            }
            if (myFollowers[p] === myFollowerTweets[p].substring(0, myFollowerTweets[p].indexOf('>'))) {
              console.log("@" + myFollowers[p].trim() + ":" + myFollowerTweets[p].substring(myFollowerTweets[p].indexOf('>') + 1));
            }
          }
        }
      }
    }
    }  
  }

