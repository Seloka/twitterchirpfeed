import { UserContentProvider } from './user-content';
import { } from 'jasmine';


describe('Test app  provider', function () {

    var theConfigProvider;
    beforeEach(function () {

        theConfigProvider = UserContentProvider;
    });

    it('tests the user provider internal function', function () {
        const result = theConfigProvider.loadUsers();
        expect(theConfigProvider).not.toBeUndefined();
        // configure the provider
        theConfigProvider.mode('local');
        // test an instance of the provider for 
        // the custom configuration changes
        expect(theConfigProvider.$get().mode).toBe('local');
    });

      it('tests the tweets provider internal function', function () {
        const result = theConfigProvider.loadTweets();
        expect(theConfigProvider).not.toBeUndefined();
        // configure the provider
        theConfigProvider.mode('local');
        // test an instance of the provider for 
        // the custom configuration changes
        expect(theConfigProvider.$get().mode).toBe('local');
    });
});
