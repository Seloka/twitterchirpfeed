import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

/*
  Generated class for the UserContentProvider provider. 
*/
@Injectable()
export class UserContentProvider {

  private usersUrl = "../../assets/files/user.txt";
  private tweetsUrl = "../../assets/files/tweet.txt";

constructor(public http: Http) {}

  public loadUsers(): Promise<any> {
    // return this.http.get('../../assets/files/user.txt') ;
    try {
      let userService = this.http.get(this.usersUrl).toPromise()
        .then(response => response)
        .catch(err => console.info("Valid Url " + err.statusText)); 
        return userService
      }
    catch (e) {
      console.log("something went wrong")
    }
  }

 ///consumes tweets from data storage 
 public loadTweets(): Promise<any> {
//return this.http.get('../../assets/files/tweet.txt');
 try {
      let error: any;
      return this.http.get(this.tweetsUrl).toPromise()
        .then(response => response)
        .catch(err => console.info("Valid Url " + err.statusText));
    }
    catch (e) {
      console.log("something went wrong")
    }   
 }
}
